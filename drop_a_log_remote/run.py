import argparse
import re
import json
import requests

class AddItemAction(argparse.Action):
    space_re = re.compile('\s+')

    def __call__(self, parser, namespace, values, option_string=None):
        key = self.space_re.sub('', values[0])
        if key == "": return False
        if not self.dest in namespace or getattr(namespace, self.dest) is None:
            setattr(namespace, self.dest, {})
        getattr(namespace, self.dest)[key] = values[1]

def parse_drop_args(args):
    """Helper for parsing command-line arguments, used by main"""
    name = args.name
    event = args.event
    key = args.key
    entry = {
        "title": args.title
    }
    def merge_args(arg):
        if arg is None: return
        if 'title' in arg: del arg['title']
        if 'date' in arg: del arg['date']
        entry.update(arg)
    merge_args(args.items)
    merge_args(args.json)

    return name, entry, event, key if (bool)(entry['title']) else False

def main():
    """entry point for drop-a-log-remote command"""
    p = argparse.ArgumentParser()
    p.add_argument("name", help='The name of the log to which this entry will be written')
    p.add_argument("title", help='The title of the entry')
    p.add_argument('--json', '-j', type=json.loads, help='Entire entry (minus title and date) as a JSON object')
    p.add_argument('--item', '-i', nargs=2, dest='items', action=AddItemAction, metavar=('key','value'), help='Add item with [value] as [key]')
    p.add_argument('--key', '-k', help='IFTTT Key')
    p.add_argument('--event', '-e', help='IFTTT Event name')

    name, entry, event, key = parse_drop_args(p.parse_args())
    if not entry:
        print("Failed to parse entry. Please check your input and try again.")
        return

    values = {
        'value1': name,
        'value2': entry['title'],
        'value3': json.dumps(entry)
    }
    data = json.dumps(values)
    url = 'https://maker.ifttt.com/trigger/%s/with/key/%s' % (event, key)

    print('Dropping %s to %s' % (data, url))
    r = requests.post(url, data = values)
    print(r.content.decode())

if __name__ == "__main__":
    main()
