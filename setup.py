#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name='drop-a-log-remote',
    description='Sending logs to droplogger via Tasker',
    url='https://github.com/goodevilgenius/droplogger',
    version='0.0.1',
    author='Dan Jones',
    author_email='danjones@goodevilgenius.org',
    license='MIT',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'drop-a-log-remote=drop_a_log_remote.run:main'
        ]
    },
    install_requires=[
        'argparse',
        'requests'
    ]
)
